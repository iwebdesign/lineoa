var express = require('express')
var http = require('http');
var app = express()
const path = require('path') // เรียกใช้งาน path module

const port = process.env.PORT || 3009  // port



const indexRouter = require('./routes/index')
// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
// respond with "hello world" when a GET request is made to the homepage
app.use(express.json())
app.use(express.urlencoded({ extended: false }))
app.use(express.static(path.join(__dirname, 'semantic')))
// app.use( express.static('node_modules/rich-filemanager') )

app.use('/', indexRouter)

// app.post('/uploadfile', function(req, res) {
//   console.log(req.body.param1); // the uploaded file object
//   res.sendStatus(200);
// });


// app.get('/', function (req, res) {
//   res.send('hello world')
// })

http.createServer(app).listen(port, function () {
     console.log('server run');
})
