const request = require('request');
const fetch = require('node-fetch')
const querystring = require('querystring');
const headers = {
  'Content-Type': 'application/json',
  'Authorization': 'Bearer FZTuyq6xSsbAUyUNoVAbyMBqjvrnhV+Wg6LnHCB1exX7AYPxgplrV0bOyoNh1j1cK5pm6z+Yry5eQ5fDbp6o5JdwXNKH/lYbQSvKQoX9dC60mtR7kc2ko8g7isFiTbdmnZ5oRwyNhfK4qgmNJODta1GUYhWQfeY8sLGRXgo3xvw='
}
exports.render  = function(req,res){
  var reply_token = req.body.events[0].replyToken;
  var itype = req.body.events[0].type;

  var re = /.*ต้องการสั่งซื้อ.*/;
  if(itype==="postback"){
    var idata =  req.body.events[0].postback.data;
    var act = querystring.parse(idata);
    var action =  act.action;
    if(action=="buy"){ //บันทึกว่าซื้อของอะไรบ้าง
      let data = {'userId':req.body.events[0].source.userId,'code_id':act.code_id,'item':act.item,'productName':act.productName,'price':act.price}
      rePlySaveBuy(reply_token,data)

    }else if(action=="cancelshop"){ // ยกเลิกการสั่งซื้อทั้งหมด
      let data = {'userId':req.body.events[0].source.userId}
      rePlyCancel(reply_token,data)

    }else{
      rePlyPostBuy(reply_token,idata)
    }

  }else{
    let msg    = req.body.events[0].message.text;

    if(re.test(msg)){
      replyConfirmBuy(reply_token,msg)
    }else{
      if(msg==="promotion."){
        replyPromotion(reply_token,msg)
      }else if(msg==="cart"){
        rePlyCart(reply_token,req.body.events[0].source.userId)
      }else{
        reply(reply_token,msg);
      }
    }
  }

  res.sendStatus(200);
};


function reply(reply_token,msg) {
  var mysql = require('mysql');
  var connection = mysql.createConnection({
    host: '150.95.27.159',
    user: 'lineoa',
    password: 'Thinker2019!',
    database: 'lineoa'
  })
  connection.query('SELECT * FROM broadcast_text where words like ?', '%' + msg + '%', function(err, result) {
    if(err){
      throw err;
    } else {
      var data = [];
      var imessage;

      var length = Object.keys(result).length;
      // console.log(   length   );

      for (var i = 0; i < length; i++)
      {

        // res.json(result[i].id);

        if(result[i].type=="text"){
          data[i] = {"type":result[i].type,"text":result[i].response_text};
        }else{

          data[i] = JSON.parse(result[i].response_text);
        }


      };

      imessage = data;


      let body = JSON.stringify({
        replyToken: reply_token,
        messages: imessage
      })
      request.post({
        url: 'https://api.line.me/v2/bot/message/reply',
        headers: headers,
        body: body
      }, (err, res, body) => {
        console.log('status = ' + res.statusCode);
      });
    }
  });


}

function replyPromotion(reply_token,msg){

  var mysql = require('mysql');
  var connection = mysql.createConnection({
    host: '150.95.27.159',
    user: 'lineoa',
    password: 'Thinker2019!',
    database: 'lineoa'
  })
  connection.query('SELECT * FROM promotion ', function(err, result) {

    if(err){
      throw err;
    } else {
      var data = [];
      var imessage;
      var length = Object.keys(result).length;
      // console.log(   length   );

      for (var i = 0; i < length; i++)
      {
        var pathImg = "https://line-oa.thinkercorp.com/uploads/"+result[i].picture;

        data[i] = {
          "type": "flex",
          "altText": "this is a flex message",
          "contents": {
            "type": "bubble",
            "size": "giga",
            "body": {
              "type": "box",
              "layout": "vertical",
              "paddingAll": "0px",
              "contents": [
                {
                  "type": "image",
                  "url": pathImg,
                  "size": "full",
                  "aspectMode": "cover",
                  "aspectRatio": "1:1",
                  "gravity": "center"
                }
              ]
              ,
              "action": {
                "type": "message",
                "label": result[i].title,
                "text": result[i].title
              }
            }
          }
        };

      }


      imessage = data;
      let textPro = [{"type": "text", "text": "โปรโมชั่นทั้งหมดของเดือนนี้ครับ"}];
      Array.prototype.push.apply(textPro,data);

      let body = JSON.stringify({
        replyToken: reply_token,
        messages: textPro
      })
      request.post({
        url: 'https://api.line.me/v2/bot/message/reply',
        headers: headers,
        body: body
      }, (err, res, body) => {
        console.log('status = ' + res.statusCode);
      });

    } // select promotion


  })


}


function getPromo(img,text){

  var pathImg = "https://line-oa.thinkercorp.com/uploads/"+img;
  var data = {
    "type": "flex",
    "altText": "Promotion",
    "contents": {
      "type": "bubble",
      "size": "giga",
      "body": {
        "type": "box",
        "layout": "vertical",
        "paddingAll": "0px",
        "contents": [
          {
            "type": "image",
            "url": pathImg,
            "size": "full",
            "aspectMode": "cover",
            "aspectRatio": "1:1",
            "gravity": "center"
          }
        ]
        ,
        "action": {
          "type": "message",
          "label": text,
          "text": text
        }
      }
    }
  }


  return data;

}




function replyConfirmBuy(reply_token,msg){


  let body = JSON.stringify({
    replyToken: reply_token,
    messages: [
      {
        "type": "template",
        "altText": "ยืนยั่นการสั่งซื้อ",
        "template": {
          "type": "confirm",
          "actions": [
            {
              "type":"postback",
              "label":"ใช่",
              "data":"action=confirmbuy&product_name="+msg,
              "text":"ใช่"
            },
            {
              "type": "message",
              "label": "ไม่",
              "text": "ไม่ซื้อ"
            }
          ],
          "text":  msg+" ?"
        }
      }
    ]
  })
  request.post({
    url: 'https://api.line.me/v2/bot/message/reply',
    headers: headers,
    body: body
  }, (err, res, body) => {
    console.log('status = ' + res.statusCode);
  });


}

function rePlyPostBuy(reply_token,idata){
  var mysql = require('mysql');
  var con = mysql.createConnection({
    host: '150.95.27.159',
    user: 'lineoa',
    password: 'Thinker2019!',
    database: 'lineoa'
  })


  var itext = querystring.parse(idata);
  var wantPro = itext.product_name;
  var input   = wantPro;
  var fields  = input.split('#');
  var code_id = fields[1];

  con.query('SELECT * FROM product where  code_id= ?',[code_id], function(err, result) {

    var productName = result[0].product_name;
    var price = result[0].price;

    let body = JSON.stringify({
      replyToken: reply_token,
      messages: [
        {
          "type": "text",
          "text": "คุณ"+wantPro+" กี่ชิ้นครับ",
          "quickReply" : {
            "items": [
              {
                "type": "action",
                "action": {
                  "type":"postback",
                  "label":"1่",
                  "data":"action=buy&code_id="+code_id+"&productName="+productName+"&item=1&price="+price,
                  "text":"1 ชิ้น"
                }
              },
              {
                "type": "action",
                "action": {
                  "type":"postback",
                  "label":"2",
                  "data":"action=buy&code_id="+code_id+"&productName="+productName+"&item=2&price="+price,
                  "text":"2 ชิ้น"
                }
              },
              {
                "type": "action",
                "action": {
                  "type":"postback",
                  "label":"3",
                  "data":"action=buy&code_id="+code_id+"&productName="+productName+"&item=3&price="+price,
                  "text":"3"
                }
              },
              {
                "type": "action",
                "action": {
                  "type":"postback",
                  "label":"4",
                  "data":"action=buy&code_id="+code_id+"&productName="+productName+"&item=4&price="+price,
                  "text":"4"
                }
              },
              {
                "type": "action",
                "action": {
                  "type":"postback",
                  "label":"5",
                  "data":"action=buy&code_id="+code_id+"&productName="+productName+"&item=5&price="+price,
                  "text":"5"
                }
              },
              {
                "type": "action",
                "action": {
                  "type":"postback",
                  "label":"6",
                  "data":"action=buy&code_id="+code_id+"&productName="+productName+"&item=6&price="+price,
                  "text":"6"
                }
              },
              {
                "type": "action",
                "action": {
                  "type":"postback",
                  "label":"7",
                  "data":"action=buy&code_id="+code_id+"&productName="+productName+"&item=7&price="+price,
                  "text":"7"
                }
              }
            ]
          }
        }
      ]
    })
    request.post({
      url: 'https://api.line.me/v2/bot/message/reply',
      headers: headers,
      body: body
    }, (err, res, body) => {
      console.log('status = ' + res.statusCode);
    });

  });



}


function checkShipping() {
  var mysql = require('mysql');
  var con = mysql.createConnection({
    host: '150.95.27.159',
    user: 'lineoa',
    password: 'Thinker2019!',
    database: 'lineoa'
  })

    con.query('SELECT * FROM setting_shop', function(err, result) { //
       if (err) throw err;

      var shipping =  result[0].shipping;
      var data = 0;

      if(shipping==0){
        data = 0;
      }else{
        data = shpping;
      }

      return data

    })


 };


// ตะกร้าสินค้าจาก richmenu
function rePlyCart(reply_token,userId){

  var mysql = require('mysql');
  var con = mysql.createConnection({
    host: '150.95.27.159',
    user: 'lineoa',
    password: 'Thinker2019!',
    database: 'lineoa'
  })



  con.query('SELECT * FROM cart where ?',{userId:userId}, async function(err, result) { //

    var receipt;
    if(err){
      receipt = [{
        'type':'text',
        'text':'ระบบขัดข้อง'
      }]
    }else{


      if(result.length>0){
          await checkShipping();
          await showCart(reply_token,userId)

      }else{

        receipt = [{
          'type':'text',
          'text':'ยังไม่มีสินค้าในตะกร้าครับ'
        }]

        let body = JSON.stringify({
          replyToken: reply_token,
          messages: receipt
        })
        request.post({
          url: 'https://api.line.me/v2/bot/message/reply',
          headers: headers,
          body: body
        }, (err, res, body) => {
          console.log('status = ' + res.statusCode);
        });

      }

    }


  })

}





function showCart(reply_token,userId){

var mysql = require('mysql');
var con = mysql.createConnection({
    host: '150.95.27.159',
    user: 'lineoa',
    password: 'Thinker2019!',
    database: 'lineoa'
})
var dateNow = Date.now();

con.connect(function(err) {
  if (err) throw err;
  con.query("SELECT * FROM cart where userId= ?",[userId], function (err, result, fields) {
    if (err) throw err;

    var reformattedArray = result.map(obj =>{
      var rObj = [];
      rObj = obj.cart_id;
      return rObj;
    });


    var da = [];
    var length = result.length
    var sumPrice = 0;
    for(var i=0;i < length;i++){
      da[i] = {"type": "box","layout": "horizontal","contents": [{"type": "text","text": result[i].product_name,"size": "sm","color": "#555555","flex": 0},{"type": "text","text": "฿"+result[i].price,"size": "sm","color": "#111111","align": "end"}]}
      sumPrice   += parseInt(result[i].price)
    }


    idata = [...da,
      {
        "type": "separator",
        "margin": "xxl"
      },
      {
        "type": "box",
        "layout": "horizontal",
        "margin": "xxl",
        "contents": [
          {
            "type": "text",
            "text": "รายการรวม",
            "size": "sm",
            "color": "#555555"
          },
          {
            "type": "text",
            "text": " "+length,
            "size": "sm",
            "color": "#111111",
            "align": "end"
          }
        ]
      },
      {
        "type": "box",
        "layout": "horizontal",
        "contents": [
          {
            "type": "text",
            "text": "ส่วนลด",
            "size": "sm",
            "color": "#555555"
          },
          {
            "type": "text",
            "text": "-",
            "size": "sm",
            "color": "#111111",
            "align": "end"
          }
        ]
      },
      {
        "type": "box",
        "layout": "horizontal",
        "contents": [
          {
            "type": "text",
            "text": "ค่าขนส่ง",
            "size": "sm",
            "color": "#555555"
          },
          {
            "type": "text",
            "text": "ฟรี",
            "size": "sm",
            "color": "#111111",
            "align": "end"
          }
        ]
      },
      {
        "type": "box",
        "layout": "horizontal",
        "contents": [
          {
            "type": "text",
            "text": "ยอดรวม",
            "size": "sm",
            "color": "#555555"
          },
          {
            "type": "text",
            "text": "฿"+sumPrice,
            "size": "sm",
            "color": "#111111",
            "align": "end"
          }
        ]
      }
    ]

    var  receipt = [{
      "type": "flex",
      "altText": "ตะกร้าสินค้าของคุณ",
      "contents": {
        "type": "bubble",
        "size":"mega",
        "body": {
          "type": "box",
          "layout": "vertical",
          "contents": [
            {
              "type": "text",
              "text": "รายการสั่งซื้อ",
              "weight": "bold",
              "color": "#1DB446",
              "size": "sm"
            },
            {
              "type": "text",
              "text": "ThinkerDev Store",
              "weight": "bold",
              "size": "xl",
              "margin": "md"
            },
            {
              "type": "text",
              "text": "ถนน พหลโยธิน แขวง ถนนพญาไท เขตราชเทวี กรุงเทพมหานคร 10400",
              "size": "xs",
              "color": "#aaaaaa",
              "wrap": true
            },
            {
              "type": "separator",
              "margin": "xxl"
            },
            {
              "type": "box",
              "layout": "vertical",
              "margin": "xxl",
              "spacing": "sm",
              "contents":idata
            },
            {
              "type": "separator",
              "margin": "xxl"
            },
            {
              "type": "box",
              "layout": "horizontal",
              "margin": "md",
              "contents": [
                {
                  "type": "text",
                  "text": "PAYMENT ID",
                  "size": "xs",
                  "color": "#aaaaaa",
                  "flex": 0
                },
                {
                  "type": "text",
                  "text": "#"+dateNow,
                  "color": "#aaaaaa",
                  "size": "xs",
                  "align": "end"
                }
              ]
            }
          ]
        },
        "footer": {
          "type": "box",
          "layout": "vertical",
          "spacing": "md",
          "contents": [
            {
              "type": "button",
              "style": "primary",
              "color": "#00897b",
              "action": {
                "type": "uri",
                "label": "แก้ไขรายการ",
                "uri": "https://linecorp.com"
              }
            },
            {
              "type": "button",
              "style": "primary",
              "color": "#0cce6b",
              "action": {
                "type": "uri",
                "label": "ชำระเงิน",
                "uri": "https://liff.line.me/1653363583-JoRDPVPA?mode=pay&page=payment&invoice="+dateNow+"&userId="+userId
              }
            },
            {
              "type": "button",
              "style": "primary",
              "color": "#d98200",
              "action": {
                "type":"postback",
                "label":"ยกเลิกรายการ",
                "data":"action=cancelshop",
                "text":"ยกเลิกรายการ"
              }
            }
          ]
        },
        "styles": {
          "footer": {
            "separator": true
          }
        }
      }
    }];


      let body = JSON.stringify({
        replyToken: reply_token,
        messages: receipt
      })
      request.post({
        url: 'https://api.line.me/v2/bot/message/reply',
        headers: headers,
        body: body
      }, (err, res, body) => {
        console.log('status = ' + res.statusCode);
      });

  });
});







}







function  rePlySaveBuy(reply_token,data){

  var mysql = require('mysql');
  var con = mysql.createConnection({
    host: '150.95.27.159',
    user: 'lineoa',
    password: 'Thinker2019!',
    database: 'lineoa'
  })

  con.query('SELECT * FROM cart where userId = ? and code_id= ?',[data.userId,data.code_id], function(err, result) {


    var numRow = result.length;

    if(numRow>0){
      // update statment

      let isum = Number(result[0].item) + Number(data.item);
      let values = [isum, result[0].cart_id];

      let sql = "UPDATE cart SET item = ? WHERE cart_id = ?";
      // execute the UPDATE statement
      con.query(sql, values , (error, results, fields) => {

        let body = JSON.stringify({
          replyToken: reply_token,
          messages: [
            {
              'type':'text',
              'text':'ใส่ตะกร้าแล้วครับ อัพเดท '
            }
          ]
        })
        request.post({
          url: 'https://api.line.me/v2/bot/message/reply',
          headers: headers,
          body: body
        }, (err, res, body) => {
          console.log('status = ' + res.statusCode);
        });


      });


    }else{



      var d = new Date();
      var n = d.toLocaleString();
      let dateNow = Date.now();
      var values = [
        [data.code_id,data.productName,data.item,data.price,data.userId,n]
      ];
      var sql = "INSERT INTO cart (code_id,product_name,item,price,userId,datetime) VALUES ?";
      con.query(sql,[values], function (err, result) {

        let body = JSON.stringify({
          replyToken: reply_token,
          messages: [
            {
              'type':'text',
              'text':'ใส่ตะกร้าแล้วครับ '
            }
          ]
        })
        request.post({
          url: 'https://api.line.me/v2/bot/message/reply',
          headers: headers,
          body: body
        }, (err, res, body) => {
          console.log('status = ' + res.statusCode);
        });

      });






    }

  })  // เช็ค ว่า มี ใน cart ป่าว


}


function rePlyCancel(reply_token,data){
  var mysql = require('mysql');
  var con = mysql.createConnection({
    host: '150.95.27.159',
    user: 'lineoa',
    password: 'Thinker2019!',
    database: 'lineoa'
  })
  let sql = "DELETE FROM cart WHERE userId = ?";

  // delete a row with id 1
  con.query(sql, [data.userId], (error, results, fields) => {
    if (error){
      return console.error(error.message);
    }

    let body = JSON.stringify({
      replyToken: reply_token,
      messages: [
        {
          'type':'text',
          'text':'ยกเลิกเรียบร้อยแล้วครับ'
        }
      ]
    })
    request.post({
      url: 'https://api.line.me/v2/bot/message/reply',
      headers: headers,
      body: body
    }, (err, res, body) => {
      console.log('status = ' + res.statusCode);
    });

  });



}
