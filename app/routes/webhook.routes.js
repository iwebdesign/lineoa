module.exports = function(app){
  var webhook = require('../controllers/webhook.controller');
  app.post('/webhook',webhook.render);
};
