module.exports = function(app){

  var index = require('../controllers/page.controller');
  app.get('/page',index.render);

  app.get('/account',function(req,res){

    res.render('account',{
      'title':'บัญชีของฉัน','content':'account','h1':'บัญชีของฉัน'
    })

  })

  app.get('/news',function(req,res){

    res.render('news',{
      'title':'ข่าวสาร','content':'news','h1':'ข่าว'
    })

  })

};
