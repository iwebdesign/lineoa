const express = require('express')
const bodyParser = require('body-parser')
const session = require('express-session')
const request = require('request')
const path = require('path')
const multer = require('multer');
const fs = require('fs')

module.exports = function(){
  var app = express();
  app.use(session({
    secret: 'secret',
    resave: false,
    saveUninitialized: false
  }));
  app.use(function(req, res, next) {
    res.locals.username = req.session.username;
    next();
  });
  app.use(bodyParser.urlencoded({extended : true}));
  app.use(bodyParser.json())
  app.set('views','./app/views');
  app.set('view engine', 'ejs');
  app.use(express.static('./public'))
  require('../app/routes/index.routes')(app);
  require('../app/routes/login.routes')(app);
  require('../app/routes/webhook.routes')(app);
  require('../app/routes/dashboard.routes')(app);
  require('../app/routes/api.routes')(app);
  require('../app/routes/broadcast.routes')(app);
  require('../app/routes/ipage.routes')(app);
  require('../app/routes/promotion.routes')(app);
  require('../app/routes/product.routes')(app);
  app.get('/dev',function(req,res){
    res.render('dev',{
      'title':'Line OA','content':'idashboard'
    })
  });

  app.post('/upload', function (req, res) {
    res.status(200)
  })


  var storage = multer.diskStorage({
    destination: function (req, file, cb) {
      cb(null, './public/uploads/')
    },
    filename: function (req, file, cb) {
      cb(null, Date.now() + '-pic' + path.extname(file.originalname));
    }
  });


  var upload = multer({ storage: storage });
  app.post('/uploadfile', upload.single('ifile'), (req, res, next) => {
    const file = req.file
    if (!file) {
      const error = new Error('Please upload a file')
      error.httpStatusCode = 400
      return next(error)
    }
    res.send(file)

  })

  app.post('/uploadfiletiny', upload.single('file'), (req, res, next) => {
    const file = req.file
    if (!file) {
      const error = new Error('Please upload a file')
      error.httpStatusCode = 400
      return next(error)
    }
    let fileName = file["filename"];
    let location = {location:'/uploads/'+fileName};

    res.json(location)


  })

  app.post('/deletePic',  (req, res) => {
    let path = './public/uploads/'+req.body.picture;
    try {
      fs.unlinkSync(path)
      res.send('ok')
    } catch(err) {
      console.error(err)
      res.send('error')
    }
  })


  app.get('/payment',  (req, res) => {
    var querystring = require('querystring');
    var obj = Object.values(req.query);

    res.send(querystring.parse(obj[0]))

  })

var fetch = require('node-fetch')
 async function itest1(url) {
   let response = await fetch("https://api.github.com/users/up1")
      let data = await response.json()
      return data
  };

  app.get('/iweb',async (req, res) => {
    var iweb11 =  await  itest1()


    var mysql = require('mysql');
    var con = mysql.createConnection({
      host: '150.95.27.159',
      user: 'lineoa',
      password: 'Thinker2019!',
      database: 'lineoa'
    })
  con.connect( await function(err) {
      if (err) throw err;
      con.query("SELECT * FROM cart where userId = ?",['U3673454cf6050b987dfbfd6aaf26d98a'], function (err, result, fields) {
        if (err) throw err;

        var da = [];
        var length = result.length;
        var sumPrice = 0;
        for(var i=0;i < length;i++){

          da[i] = {"type": "box","layout": "horizontal","contents": [{"type": "text","text": result[i].product_name,"size": "sm","color": "#555555","flex": 0},{"type": "text","text": "฿"+result[i].price,"size": "sm","color": "#111111","align": "end"}]}
          sumPrice   += parseInt(result[i].price)
        }
        var itest = [{"test":da}]
        var mer   = [{"it":'ttt'}]

        var idata =  [{
          "type": "separator",
          "margin": "xxl"
        },
        {
          "type": "box",
          "layout": "horizontal",
          "margin": "xxl",
          "contents": [
            {
              "type": "text",
              "text": "รวมรายการ",
              "size": "sm",
              "color": "#555555"
            },
            {
              "type": "text",
              "text": length,
              "size": "sm",
              "color": "#111111",
              "align": "end"
            }
          ]
        },
        {
          "type": "box",
          "layout": "horizontal",
          "contents": [
            {
              "type": "text",
              "text": "ส่วนลด",
              "size": "sm",
              "color": "#555555"
            },
            {
              "type": "text",
              "text": "-",
              "size": "sm",
              "color": "#111111",
              "align": "end"
            }
          ]
        },
        {
          "type": "box",
          "layout": "horizontal",
          "contents": [
            {
              "type": "text",
              "text": "ค่าขนส่ง",
              "size": "sm",
              "color": "#555555"
            },
            {
              "type": "text",
              "text": "-",
              "size": "sm",
              "color": "#111111",
              "align": "end"
            }
          ]
        },
        {
          "type": "box",
          "layout": "horizontal",
          "contents": [
            {
              "type": "text",
              "text": "ยอดรวม",
              "size": "sm",
              "color": "#555555"
            },
            {
              "type": "text",
              "text": "฿"+sumPrice,
              "size": "sm",
              "color": "#111111",
              "align": "end"
            }
          ]
        }]

        var array3 = [...da, ...idata];
        // console.log(JSON.parse(da));
        var reformattedArray = result.map(obj =>{
          var rObj = [];
          rObj = obj.cart_id;
          return rObj;
        });
        if(result[0].invoice==null || result[0].invoice==""){
          var iweb = 'yest';
        }
        // res.setHeader('Content-Type', 'application/json');
        res.send('good')

      });
    });

  })




  app.post('/checkapi',async (req, res) => {
      var mysql = require('mysql')
    var connection = mysql.createConnection({
      host: '150.95.27.159',
      user: 'lineoa',
      password: 'Thinker2019!',
      database: 'lineoa'
    })

    connection.query('SELECT * FROM promocode WHERE ?',{userId:'U3673454cf6050b987dfbfd6aaf26d98a'}, function(err, result) {
         if(err){
             throw err;
         } else {
            // obj = result;

               if(result.length>0){
                 res.json(['ok'])
               }else{
                 res.json(['no'])

               }
                res.status(200)

         }
     });
  })





  return app;
}
